# Dependency Injection Container Project with Nest.js Framework

## Description

This project serves as an example of using a Dependency Injection (DI) container in an application built on the Nest.js framework. DI is a popular approach to managing dependencies in applications, making it easy to inject dependencies into classes and providing a more flexible project structure.

## Functionality

- **Dependency Injection Container:** The project contains a configured DI container, allowing registration and resolution of dependencies within the Nest.js application.
- **Usage Examples:** The project provides examples of using the DI container to inject dependencies into various components of the Nest.js application.

## Requirements

The project is developed based on the Nest.js framework and can be run on platforms supported by Nest.js.

## Installation and Running

1. **Clone the Repository:** Clone the repository on your local machine using the following command:

    ```bash
    git clone https://gitlab.com/SergMuhin/nest-di-container.git
    ```

2. **Install Dependencies:** Navigate to the project directory and install dependencies using the following command:

    ```bash
    npm install
    ```
3. **Run the Application:** After configuring the DI container, start your Nest.js application using the command:

    ```bash
    npm start
    ```
